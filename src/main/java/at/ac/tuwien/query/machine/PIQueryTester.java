package at.ac.tuwien.query.machine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.apache.jena.riot.RDFDataMgr;

import at.ac.tuwien.query.helper.OwlHelper;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;

/**
 * PI Query Tester
 *  Designed to work with a custom inference engine & queries in files
 *   
 * @author Juang fajar.juang@gmail.com
 *
 */
public class PIQueryTester {
    private OntModel model;
    private String defaultURI;
    private Dataset dataset;
    private OutputStream outputStream;
    
    /**
     * Constructor.
     * 
     * @param owlFile owlfile name.
     * @param ruleFile custom rule file.
     */
    public PIQueryTester(String owlFile, String ruleFile, boolean isUsingFile) {
        model = null;
        dataset = RDFDataMgr.loadDataset(owlFile);
        defaultURI = dataset.getDefaultModel().getNsPrefixURI("");
        if(!isUsingFile) {
            outputStream = System.out;
        } else {
            try { 
                SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy_HH:mm");
                Date resultdate = new Date(System.currentTimeMillis());
                outputStream = System.out;
                outputStream = new FileOutputStream("result/output_"+sdf.format(resultdate)+".txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        
        initRules(ruleFile);
    }
        
    /**
     * Query executor.
     * 
     * @param fileName query file name
     * @param params parameters to replace params in the query. 
     *  E.g., if you have a pair of string "pou" & "T_Pump", it will replace all ?pou with string "T_Pump"
     * @param debug if this is true, will print SPARQL query in the terminal
     */
    public void QueryExec(String fileName, Map<String, String> params, boolean debug) {
        ParameterizedSparqlString qString = new ParameterizedSparqlString();
        qString.setNsPrefixes(model.getNsPrefixMap());
        formulateQuery(qString, fileName);
        if( params!=null && !params.isEmpty() ) setParam(qString, params);
        doQueryExec(qString, debug);
    }
    
    private void formulateQuery(ParameterizedSparqlString qString, String fileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String brLine;
            while((brLine = br.readLine()) != null) {
                if(brLine.startsWith("#")) {
                    outputStream.write(brLine.concat("\n").getBytes());
                }
                else qString.append(brLine);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void doQueryExec(ParameterizedSparqlString qString, boolean debug) {
        if(debug) System.out.println(qString.toString());

        long execTime = System.currentTimeMillis();
        QueryExecution qe = QueryExecutionFactory.create(qString.asQuery(), model);
        ResultSet rs = qe.execSelect();
        ResultSetFormatter.out(outputStream, rs);
        execTime = System.currentTimeMillis() - execTime;
        try {
            outputStream.write(("execution time: "+Long.toString(execTime)+" ms \n\n").getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        qe.close();
    }
    
    private void setParam(ParameterizedSparqlString qString, Map<String, String> params) {
        Iterator<String> paramIter = params.keySet().iterator();
        while(paramIter.hasNext()) {
            String key = paramIter.next();
            String value = params.get(key);
            qString.setParam(key,  ResourceFactory.createPlainLiteral(value));
        }
    }
    
    private void initRules(String ruleFile) {
        Model m = dataset.getDefaultModel();
        Reasoner reasoner = new GenericRuleReasoner(OwlHelper.readRules(defaultURI, ruleFile));
        reasoner = reasoner.bindSchema(m);
        OntModelSpec ontModelSpec = OntModelSpec.OWL_DL_MEM;
        ontModelSpec.setReasoner(reasoner);
        model = ModelFactory.createOntologyModel(ontModelSpec, m);
    }
}
