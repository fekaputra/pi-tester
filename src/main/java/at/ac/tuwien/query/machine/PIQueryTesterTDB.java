package at.ac.tuwien.query.machine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.jena.riot.RDFDataMgr;

import at.ac.tuwien.query.helper.OwlHelper;

import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.rulesys.GenericRuleReasoner;
import com.hp.hpl.jena.tdb.TDB;
import com.hp.hpl.jena.tdb.TDBFactory;
import com.hp.hpl.jena.util.FileManager;

/**
 * PI Query Tester
 *  Designed to work with a custom inference engine & queries in files
 *   
 * @author Juang fajar.juang@gmail.com
 *
 */
public class PIQueryTesterTDB {
    String directory = "tdb/db1" ;
    
    private Model model;
    private String defaultURI;
    private Dataset tdbDataset;
    private Dataset modelDataset;
    
    /**
     * Constructor.
     * 
     * @param owlFile owlfile name.
     * @param ruleFile custom rule file.
     */
    public PIQueryTesterTDB(String owlFile, String ruleFile) {
        tdbDataset = TDBFactory.createDataset(directory);
        model = tdbDataset.getDefaultModel();
        modelDataset = RDFDataMgr.loadDataset(owlFile);
        defaultURI = model.getNsPrefixURI("");
        initRules(ruleFile);
    }
    
    public void closeTDB() {
        TDB.sync(tdbDataset);
        tdbDataset.close();
        System.gc();
    }
    
    public static void main(String[] args) {
        String directory = "tdb/db1" ;
        String source = "owl/big_project.owl";
        
        Dataset tdbDataset = TDBFactory.createDataset(directory) ;
        Model tdb = tdbDataset.getDefaultModel();
        FileManager.get().readModel( tdb, source);
        TDB.sync(tdbDataset);
        
        tdbDataset.close() ;
    }
        
    /**
     * Query executor.
     * 
     * @param fileName query file name
     * @param params parameters to replace params in the query. 
     *  E.g., if you have a pair of string "pou" & "T_Pump", it will replace all ?pou with string "T_Pump"
     * @param debug if this is true, will print SPARQL query in the terminal
     */
    public void QueryExec(String fileName, Map<String, String> params, boolean debug) {
        ParameterizedSparqlString qString = new ParameterizedSparqlString();
        qString.setNsPrefixes(model.getNsPrefixMap());
        formulateQuery(qString, fileName);
        if( params!=null && !params.isEmpty() ) setParam(qString, params);
        doQueryExec(qString, debug);
    }
    
    private void formulateQuery(ParameterizedSparqlString qString, String fileName) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String brLine;
            while((brLine = br.readLine()) != null) {
                if(brLine.startsWith("#")) System.out.println(brLine); 
                else qString.append(brLine);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void doQueryExec(ParameterizedSparqlString qString, boolean debug) {
        if(debug) System.out.println(qString.toString());
        
        long execTime = System.currentTimeMillis();
        QueryExecution qe = QueryExecutionFactory.create(qString.asQuery(), model);
        execTime = System.currentTimeMillis() - execTime;
        ResultSetFormatter.out(System.out, qe.execSelect(), qString);
        System.out.println("time: "+Long.toString(execTime)+" ms");
        System.out.println();
        
        qe.close();
    }
    
    private void setParam(ParameterizedSparqlString qString, Map<String, String> params) {
        Iterator<String> paramIter = params.keySet().iterator();
        while(paramIter.hasNext()) {
            String key = paramIter.next();
            String value = params.get(key);
            qString.setParam(key,  ResourceFactory.createPlainLiteral(value));
        }
    }
    
    private void initRules(String ruleFile) {
        Reasoner reasoner = new GenericRuleReasoner(OwlHelper.readRules(defaultURI, ruleFile));
        reasoner = reasoner.bindSchema(modelDataset.getDefaultModel());
        OntModelSpec ontModelSpec = OntModelSpec.OWL_DL_MEM;
        ontModelSpec.setReasoner(reasoner);
        model = ModelFactory.createOntologyModel(ontModelSpec, model);
    }
}
