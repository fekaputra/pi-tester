package at.ac.tuwien.query.main;

//import at.ac.tuwien.query.machine.PIQueryTester;
import at.ac.tuwien.query.machine.PIQueryTesterMulti;

public class Main {
    
    public static void main(String[] args) {
        PIQueryTesterMulti tester = new PIQueryTesterMulti("owl/HYDTEST.tc6.owl", "owl/HYDTEST.esm.owl", "rules/pi-new.rules", false);
//        tester.showAllPrefixes();
//        PIQueryTester tester = new PIQueryTester("owl/HYDTEST.L2P.wze.owl", "rules/pi-new.rules", true);

        tester.QueryExec("queries/_q0.sparql", null, false);
//        tester.QueryExec("queries/_q3.sparql", null, false);
//        tester.QueryExec("queries/_q4.sparql", null, false);
//        tester.QueryExec("queries/_q6.sparql", null, false);
//        tester.QueryExec("queries/_q9.sparql", null, false);
//        tester.QueryExec("queries/_q11.sparql", null, false);
//        tester.QueryExec("queries/_q12.sparql", null, false);
//        tester.QueryExec("queries/_q13.sparql", null, false);
//        tester.QueryExec("queries/_q14.sparql", null, false);
//        tester.QueryExec("queries/_q15.sparql", null, false);
        tester.QueryExec("queries/_q16.sparql", null, false);
        
        
    }
}
